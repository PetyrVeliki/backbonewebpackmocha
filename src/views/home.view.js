var $ = require('jquery');
var Backbone = require('backbone');

var homeHtml = require('html!./templates/home.view.html');

module.exports = Backbone.View.extend({
	el: $('#content'),
	initialize: function() {
		this.render();
	},
	render: function() {
		this.$el.html(homeHtml);
		return this;
	}
});

