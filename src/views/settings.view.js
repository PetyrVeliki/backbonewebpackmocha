var $ = require('jquery');
var Backbone = require('backbone');

var settingsHtml = require('html!./templates/settings.view.html');

module.exports = Backbone.View.extend({
	el: $('#content'),
	initialize: function() {
		this.render();
	},
	render: function() {
		this.$el.html(settingsHtml);
		return this;
	}
});
