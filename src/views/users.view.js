var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');

var usersHtml = require('html!./templates/users.view.html');
var userModel = require('../models/user.model');
var usersCollection = require('../collections/users.collection');

module.exports = Backbone.View.extend({
	el: $('#content'),
    allUsers: {},
	initialize: function() {

        var self = this;

        this.allUsers = new usersCollection();

        // call the endpoint for the users .. 
        $.get('https://jsonplaceholder.typicode.com/users', function(users) {

            if (users && users.length > 0) {
                _.each(users, function(user) {
                    self.allUsers.push(
                        new userModel({
                            id: user.id,
                            name: user.name,
                            phone: user.phone,
                            username: user.username,
                            avatar: "https://placebear.com/40/40"
                        })
                    );
                })
            }

		    self.render();

        }); // end of $.get .. 

	},
	render: function() {

        var compiled = _.template(usersHtml);

		this.$el.html(
            compiled({allUsers: this.allUsers.models})
        );

		return this;
	}
});