module.exports = Backbone.Model.extend({
    defaults: {
        id: 0,
        name: "",
        phone: "",
        username: "",
        avatar: ""
    }
});