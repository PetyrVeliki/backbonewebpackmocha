// Import backbone and it's dependencies ..
var Backbone = require('backbone');

// Import the views for home and settings screens .. 
var HomeView = require('./views/home.view');
var SettingsView = require('./views/settings.view');
var UsersView = require('./views/users.view');

// Define our routes .. 
var Router = Backbone.Router.extend({
	initialize: function() {
		Backbone.history.start();
	},
	routes: {
		"settings": "settings",
		"users": "users",
		"home": "home",
		"": "home"
	},
	home: function() {
		new HomeView();
	},
	settings: function() {
		new SettingsView();
	},
	users: function() {
		new UsersView();
	}
});

// And kick off our app .. 
new Router();

