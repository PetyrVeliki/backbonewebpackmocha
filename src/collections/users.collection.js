var userModel = require('../models/user.model');

module.exports = Backbone.Collection.extend({
    model: userModel
});