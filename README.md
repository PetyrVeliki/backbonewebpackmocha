# Backbone dev environment on top of Webpack

### Installation

1. Install the dependencies by running:
>npm install


2. Install also `webpack` and `mocha` globally, to avoid some issues across platforms or distributions:
> [sudo] npm install webpack mocha -g

### Directories

> public

The docroot of the app. Also - place where final bundle  is generated. Can serve directly as docroot in a PHP app, for example.

> src

All the source files. Loading of files happens in two manners:

* JS - load in the CommonJS module manner.
* HTML templates - load with the `text` loader of `webpack`.

> tests

All the unit tests are there, duplicating - as much as possible - the structure of the source files.

### Usage

You have three available commands:

> npm run start

This command runs the dev server on port `8080`. Then you open `http://localhost:8080` with the sample mini-app. When you edit code - the browser automatically refreshes.

> npm run prod

Generated the production-ready minified bundle with all the JS code inside and places it in the public folder.

> npm run test

Runs another web server with results from all the unit tests. When you edit test - the page with the results automatically refreshes.
