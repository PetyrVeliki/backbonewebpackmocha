var webpack = require('webpack');

var GLOBALS = {
	'process.env.NODE_ENV': JSON.stringify('production')
};

var DevConfig = require('./webpack.config.js');

DevConfig.plugins = [
	new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.DefinePlugin(GLOBALS),
	new webpack.optimize.DedupePlugin(),
	new webpack.optimize.UglifyJsPlugin()
];

module.exports = DevConfig;
