module.exports = {
	entry: "./src/main.js",
	output: {
		path: "./public",
		filename: "app.js",
		publicPath: "/"
	},
	devServer: {
		inline: true,
		contentBase: "./public"
	}
};
