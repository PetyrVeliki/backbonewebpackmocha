// Import our assertion library .. 
var should = require('should');

describe('Application', function() {

    it('Should have Backbone available', function() {
        should.exist(Backbone);
    });

});

// And load all other specific tests - for views, models, etc. ..

// Models ..  
require('./models/user.model.test');

// Views .. 
require('./views/user.views.test');