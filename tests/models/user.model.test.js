var userModel = require('../../src/models/user.model');

var sampleUser = new userModel();

describe('Model', function() {
 
	it('Should have empty default ID', function() {
		sampleUser.get('id').should.equal(0);
	});
 
	it('Should have empty default name', function() {
		sampleUser.get('name').should.equal('');
	});
 
	it('Should have empty default phone', function() {
		sampleUser.get('phone').should.equal('');
	});
 
	it('Should have empty default username', function() {
		sampleUser.get('username').should.equal('');
	});
 
	it('Should have empty default avatar', function() {
		sampleUser.get('avatar').should.equal('');
	});

	var newUser = new userModel({
		id: 1,
		name: 'Name',
		phone: 'Phone',
		username: 'Username',
		avatar: "sample.jpg"
	});

	it('Should have correct ID', function() {
		newUser.get('id').should.equal(1);
	});

	it('Should have correct name', function() {
		newUser.get('name').should.equal('Name');
	});

	it('Should have correct phone', function() {
		newUser.get('phone').should.equal('Phone');
	});

	it('Should have correct name', function() {
		newUser.get('username').should.equal('Username');
	});

	it('Should have correct avatar', function() {
		newUser.get('avatar').should.equal('sample.jpg');
	});

});