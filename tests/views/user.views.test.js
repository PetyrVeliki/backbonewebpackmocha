var usersView = require('../../src/views/users.view');

describe('View', function() {

	beforeEach(function() {
		this.sampleUsersView = new usersView();
	});

	it("render() should return the object", function() {
		this.sampleUsersView.render().should.equal(this.sampleUsersView);
	});

});